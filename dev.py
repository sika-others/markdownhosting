#!/usr/bin/python
# coding: utf8
# create by flask-init (Ondrej Sika)

from app import app

if __name__ == "__main__":
    app.debug = True
    app.run()