main.factory 'globals', ($rootScope) ->
    globals = {}
    globals.storage = {}
    globals.set = (key, val) ->
        this.storage[key] = val 
        $rootScope.$broadcast 'handleGlobals'
    globals.get = (key) -> this.storage[key]
    return globals