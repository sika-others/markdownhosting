ProjectController = ($scope, $routeParams, $http) ->
    $scope.routeParams = $routeParams

    $http.get("/api/user-"+$routeParams["user"]+"/project-"+$routeParams["project"]+"/")
        .success (data) -> $scope.project_data = data