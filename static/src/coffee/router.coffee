main.config ($routeProvider) -> 
    $routeProvider
    # .when("/", {
    #    templateUrl: "/static/templates/home.html",
    # })
    .when("/", {
       redirectTo: "/about",
    })
    .when("/about", {
       templateUrl: "/static/templates/about.html",
    })
    .when("/:user/", {
       templateUrl: "/static/templates/user.html",
       controller: UserController,
    })
    .when("/:user/:project/", {
       templateUrl: "/static/templates/project.html",
       controller: ProjectController,
    })
    .when("/:user/:project/:page/", {
       templateUrl: "/static/templates/page.html",
       controller: PageController,
    })
    .when("/:user/:project/:page/:pageOption/", {
       templateUrl: "/static/templates/page.html",
       controller: PageController,
    })
    .otherwise {redirectTo: "/"}