UserController = ($scope, $routeParams, $http) ->
    $scope.routeParams = $routeParams;

    $http.get("/api/user-"+$routeParams["user"]+"/")
        .success (data) -> $scope.user_data = data
