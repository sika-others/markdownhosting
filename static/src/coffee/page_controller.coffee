PageController = ($scope, $routeParams, $http) ->
    $scope.routeParams = $routeParams;

    $scope.md = {}
    $http.get("/static/storage/"+$routeParams["user"]+"/"+$routeParams["project"]+"/"+$routeParams["page"]+".md")
        .success (data) -> 
            $scope.md.raw = data
            $scope.md.html = markdown.toHTML $scope.md.raw
