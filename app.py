#!/usr/bin/python
# coding: utf8
# create by flask-init (Ondrej Sika)

try:
    execfile('env/bin/activate_this.py', dict(__file__='env/bin/activate_this.py'))
except IOError:
    print "no virtulenv in PROJECT_ROOT/env"
    pass

import os
import json

from flask import Flask
app = Flask(__name__)

DATA = "data/"
STORAGE = os.path.join(DATA, "storage")


@app.route("/")
def index():
    return file("index.html").read()

def reduce_user(iterable, filename):
    if filename[0] != ".":
        iterable.append(filename)
    return iterable

@app.route("/api/user-<user>/")
def user(user):
    user_exists = False
    projects = []
    if os.path.exists(os.path.join(STORAGE, user)):
        user_exists = True
        projects = reduce(reduce_user, sorted(os.listdir(os.path.join(STORAGE, user))), [])

    return json.dumps({
        "user_exists": user_exists,
        "projects": projects,
    })

def reduce_project(iterable, filename):
    if filename[0] != ".":
        iterable.append(filename.rstrip(".md"))
    return iterable

@app.route("/api/user-<user>/project-<project>/")
def project(user, project):
    project_exists = False
    files = []
    if os.path.exists(os.path.join(STORAGE, user, project)):
        project_exists = True
        files = reduce(reduce_project, sorted(os.listdir(os.path.join(STORAGE, user, project))), [])

    return json.dumps({
        "project_exists": project_exists,
        "files": files,
    })

if __name__ == "__main__":
    app.run()